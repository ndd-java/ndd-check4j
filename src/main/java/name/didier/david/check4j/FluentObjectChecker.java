package name.didier.david.check4j;

/**
 * Checking methods for {@link Object}s. To create a new instance of this class, invoke
 * <code>{@link name.didier.david.check4j.FluentCheckers#checkThat(Object)}</code>.
 *
 * @param <A> the type of the "actual" value.
 *
 * @author ddidier
 * @author assertj
 */
public class FluentObjectChecker<A>
        extends AbstractFluentChecker<FluentObjectChecker<A>, A> {

    /**
     * Hidden constructor.
     *
     * @param actual the actual value to check.
     */
    protected FluentObjectChecker(final A actual) {
        super(actual, FluentObjectChecker.class);
    }
}
