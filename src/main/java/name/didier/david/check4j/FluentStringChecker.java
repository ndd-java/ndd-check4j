package name.didier.david.check4j;

/**
 * Checking methods for {@link String}s. To create a new instance of this class, invoke
 * <code>{@link name.didier.david.check4j.FluentCheckers#checkThat(String)}</code>.
 * 
 * @author ddidier
 * @author assertj
 */
public class FluentStringChecker
        extends AbstractFluentCharSequenceChecker<FluentStringChecker, String> {

    /**
     * Hidden constructor.
     * 
     * @param actual the actual value to check.
     */
    protected FluentStringChecker(final String actual) {
        super(actual, FluentStringChecker.class);
    }

    @Override
    public FluentStringChecker isNotBlank() {
        isNotNull();
        if (actual.trim().length() == 0) {
            throw expectedParameterTo("not be blank but was <%s>", actual);
        }
        return myself;
    }
}
