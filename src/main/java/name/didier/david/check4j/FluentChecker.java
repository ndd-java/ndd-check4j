package name.didier.david.check4j;

/**
 * Base contract of all checker objects: the minimum functionality that any checker object should provide.
 *
 * @param <C> the "self" type of this checker class.
 * @param <A> the type of the "actual" value.
 *
 * @author ddidier
 * @author assertj
 */
public interface FluentChecker<C extends FluentChecker<C, A>, A> {

    /**
     * Sets the name associated with the parameter to check.
     *
     * @param name the name of the parameter.
     * @return {@code this} object.
     */
    C as(String name);

    /**
     * Returns the actual value.
     *
     * @return the actual value.
     */
    A thenAssign();

    /**
     * Verifies that the actual value is not {@code null}.
     *
     * @return {@code this} checker object.
     * @throws IllegalArgumentException if the actual value is {@code null}.
     */
    C isNotNull();

    /**
     * Verifies that the actual value is an instance of the given type.
     *
     * @param type the type to check the actual value against.
     * @return {@code this} checker object.
     * @throws IllegalArgumentException if the actual value is {@code null} or is not an instance of the given type.
     */
    C isInstanceOf(Class<?> type);
}
