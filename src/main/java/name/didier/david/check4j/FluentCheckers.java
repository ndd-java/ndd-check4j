package name.didier.david.check4j;

import java.util.Collection;

/**
 * Entry point for checking methods for different data types. Each method in this class is a static factory for the
 * type-specific checker objects. The purpose of this class is to make code more readable. For example:
 *
 * <pre>
 * {@link FluentCheckers#checkThat(Object) checkThat}(param1).{@link FluentObjectChecker#isNotNull isNotNull}();
 * </pre>
 *
 * @see ConciseCheckers
 *
 * @author assertj
 * @author ddidier
 */
public class FluentCheckers {

    /**
     * Default constructor.
     */
    protected FluentCheckers() {
        super();
    }

    /**
     * Creates a new instance of <code>{@link FluentObjectChecker}</code>.
     *
     * @param <T> the type of the actual value.
     * @param actual the actual value.
     * @return the created checker object.
     */
    public static <T> FluentObjectChecker<T> checkThat(final T actual) {
        return new FluentObjectChecker<>(actual);
    }

    /**
     * Creates a new instance of <code>{@link FluentStringChecker}</code>.
     *
     * @param actual the actual value.
     * @return the created checker object.
     */
    public static FluentStringChecker checkThat(final String actual) {
        return new FluentStringChecker(actual);
    }

    /**
     * Creates a new instance of <code>{@link FluentIntegerChecker}</code>.
     *
     * @param actual the actual value.
     * @return the created checker object.
     */
    public static FluentIntegerChecker checkThat(final Integer actual) {
        return new FluentIntegerChecker(actual);
    }

    /**
     * Creates a new instance of <code>{@link FluentLongChecker}</code>.
     *
     * @param actual the actual value.
     * @return the created checker object.
     */
    public static FluentLongChecker checkThat(final Long actual) {
        return new FluentLongChecker(actual);
    }

    /**
     * Creates a new instance of <code>{@link FluentFloatChecker}</code>.
     *
     * @param actual the actual value.
     * @return the created checker object.
     */
    public static FluentFloatChecker checkThat(final Float actual) {
        return new FluentFloatChecker(actual);
    }

    /**
     * Creates a new instance of <code>{@link FluentDoubleChecker}</code>.
     *
     * @param actual the actual value.
     * @return the created checker object.
     */
    public static FluentDoubleChecker checkThat(final Double actual) {
        return new FluentDoubleChecker(actual);
    }

    /**
     * Creates a new instance of <code>{@link FluentObjectArrayChecker}</code>.
     *
     * @param <T> the type of the actual value.
     * @param actual the actual value.
     * @return the created checker object.
     */
    public static <T> FluentObjectArrayChecker<T> checkThat(final T[] actual) {
        return new FluentObjectArrayChecker<>(actual);
    }

    /**
     * Creates a new instance of <code>{@link FluentCollectionChecker}</code>.
     *
     * @param <C> the type of the actual collection.
     * @param <T> the type of the actual value.
     * @param actual the actual value.
     * @return the created checker object.
     */
    public static <C extends Collection<T>, T> FluentCollectionChecker<C, T> checkThat(final C actual) {
        return new FluentCollectionChecker<>(actual);
    }
}
