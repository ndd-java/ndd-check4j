package name.didier.david.check4j;

/**
 * Checking methods for {@link Integer}s. To create a new instance of this class, invoke
 * <code>{@link name.didier.david.check4j.FluentCheckers#checkThat(Integer)}</code>.
 *
 * @author ddidier
 * @author assertj
 */
public class FluentIntegerChecker
        extends AbstractFluentNumberChecker<FluentIntegerChecker, Integer> {

    /**
     * Abstract constructor.
     *
     * @param actual the actual value to check.
     */
    protected FluentIntegerChecker(final Integer actual) {
        super(actual, FluentIntegerChecker.class);
    }

    @Override
    protected Integer zero() {
        return Integer.valueOf(0);
    }
}
