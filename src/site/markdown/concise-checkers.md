# Concise checkers

Check4J provides concise constructs to help checking parameters.

## Object checks
 
These checks are inherited by all other checks.

```java
this.myObject = checkNotNull(anObject, "myObject");
this.myObject = checkInstanceOf(anObject, Object.class, "myObject");
```

## String checks
 
```java
this.myString = checkNotEmpty(aString, "myString");
this.myString = checkNotBlank(aString, "myString");
```

## Number checks
 
Number checks are currently implemented for `Integer`, `Long`, `Float` and `Double`.

```java
this.myInteger = checkNegative(anInt, "myInteger");
this.myInteger = checkStrictlyNegative(anInt, "myInteger");
this.myInteger = checkPositive(anInt, "myInteger");
this.myInteger = checkStrictlyPositive(anInt, "myInteger");
this.myInteger = checkLessThan(anInt, 1, "myInteger");
this.myInteger = checkStrictlyLessThan(anInt, 1, "myInteger");
this.myInteger = checkGreaterThan(anInt, 1, "myInteger");
this.myInteger = checkStrictlyGreaterThan(anInt, 1, "myInteger");
this.myInteger = checkBetween(anInt, -1, 1, "myInteger");
this.myInteger = checkStrictlyBetween(anInt, -1, 1, "myInteger");
```

## Array checks
 
```java
this.myArray = checkNotEmpty(anArray, "myArray");
```

## Collection checks
 
```java
this.myCollection = checkNotEmpty(aCollection, "myCollection");
```
