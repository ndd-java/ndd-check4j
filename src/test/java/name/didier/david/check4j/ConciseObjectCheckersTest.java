package name.didier.david.check4j;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;

import static name.didier.david.check4j.ConciseCheckers.checkInstanceOf;
import static name.didier.david.check4j.ConciseCheckers.checkNotNull;

import org.testng.annotations.Test;

@Test
public class ConciseObjectCheckersTest
        extends AbstractConciseCheckersTestBase {

    private static final Object OBJECT = new Object();
    private static final ParentClass CHILD_AS_PARENT = new ChildClass();

    // -----------------------------------------------------------------------------------------------------------------

    public void checkNotNull_should_pass_if_not_null_then_return_parameter() {
        assertThat(checkNotNull(OBJECT)).isSameAs(OBJECT);
    }

    public void checkNotNull_should_pass_if_not_null_then_return_parameter_with_pName() {
        assertThat(checkNotNull(OBJECT, P_NAME)).isSameAs(OBJECT);
    }

    public void checkNotNull_should_fail_if_null() {
        try {
            checkNotNull(null);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(NULL_MESSAGE);
        }
    }

    public void checkNotNull_should_fail_if_null_with_pName() {
        try {
            checkNotNull(null, P_NAME);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(NULL_P_MESSAGE);
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    public void checkInstanceOf_should_pass_if_not_null_and_instance_of_class_then_return_parameter() {
        assertThat(checkInstanceOf(OBJECT, Object.class)).isSameAs(OBJECT);
    }

    public void checkInstanceOf_should_pass_if_not_null_and_instance_of_class_then_return_parameter_with_pName() {
        assertThat(checkInstanceOf(OBJECT, Object.class, P_NAME)).isSameAs(OBJECT);
    }

    @SuppressWarnings("unused")
    public void checkInstanceOf_should_cast_result_if_instance_of_class() {
        Object cast = checkInstanceOf(OBJECT, Object.class);
    }

    public void checkInstanceOf_should_pass_if_not_null_and_instance_of_superclass_then_return_parameter() {
        assertThat(checkInstanceOf(CHILD_AS_PARENT, ParentClass.class)).isSameAs(CHILD_AS_PARENT);
    }

    public void checkInstanceOf_should_pass_if_not_null_and_instance_of_superclass_then_return_parameter_with_pName() {
        assertThat(checkInstanceOf(CHILD_AS_PARENT, ParentClass.class, P_NAME)).isSameAs(CHILD_AS_PARENT);
    }

    @SuppressWarnings("unused")
    public void checkInstanceOf_should_cast_result_if_instance_of_superclass() {
        ParentClass cast = checkInstanceOf(CHILD_AS_PARENT, ParentClass.class);
    }

    public void checkInstanceOf_should_fail_if_null() {
        try {
            checkInstanceOf(null, Object.class);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(NULL_MESSAGE);
        }
    }

    public void checkInstanceOf_should_fail_if_null_with_pName() {
        try {
            checkInstanceOf(null, Object.class, P_NAME);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(NULL_P_MESSAGE);
        }
    }

    public void checkInstanceOf_should_fail_if_not_instance_of() {
        try {
            checkInstanceOf(OBJECT, String.class);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Expected parameter to be an instance of <java.lang.String>\n"
                    + "but was instance of <java.lang.Object>");
        }
    }

    public void checkInstanceOff_should_fail_if_not_instance_of_with_pName() {
        try {
            checkInstanceOf(OBJECT, String.class, P_NAME);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Expected parameter 'P' to be an instance of <java.lang.String>\n"
                    + "but was instance of <java.lang.Object>");
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    private static class ParentClass {
        ParentClass() {
            super();
        }
    }

    private static class ChildClass
            extends ParentClass {
        ChildClass() {
            super();
        }
    }
}
