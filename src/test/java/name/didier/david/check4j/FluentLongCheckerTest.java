package name.didier.david.check4j;

import static java.lang.Long.valueOf;

import org.testng.annotations.Test;

@Test
public class FluentLongCheckerTest
        extends AbstractFluentNumberCheckerTestBase<FluentLongChecker, Long> {

    // -----------------------------------------------------------------------------------------------------------------

    @Override
    protected FluentLongChecker newChecker(final Long actual) {
        return new FluentLongChecker(actual);
    }

    @Override
    protected Long numberN() {
        return valueOf(-1);
    }

    @Override
    protected Long numberZ() {
        return valueOf(0);
    }

    @Override
    protected Long numberP() {
        return valueOf(1);
    }
}
