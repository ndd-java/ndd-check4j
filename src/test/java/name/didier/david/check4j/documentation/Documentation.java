package name.didier.david.check4j.documentation;

import static com.google.common.collect.Lists.newArrayList;

import static name.didier.david.check4j.ConciseCheckers.checkBetween;
import static name.didier.david.check4j.ConciseCheckers.checkGreaterThan;
import static name.didier.david.check4j.ConciseCheckers.checkInstanceOf;
import static name.didier.david.check4j.ConciseCheckers.checkLessThan;
import static name.didier.david.check4j.ConciseCheckers.checkNegative;
import static name.didier.david.check4j.ConciseCheckers.checkNotBlank;
import static name.didier.david.check4j.ConciseCheckers.checkNotEmpty;
import static name.didier.david.check4j.ConciseCheckers.checkNotNull;
import static name.didier.david.check4j.ConciseCheckers.checkPositive;
import static name.didier.david.check4j.ConciseCheckers.checkStrictlyBetween;
import static name.didier.david.check4j.ConciseCheckers.checkStrictlyGreaterThan;
import static name.didier.david.check4j.ConciseCheckers.checkStrictlyLessThan;
import static name.didier.david.check4j.ConciseCheckers.checkStrictlyNegative;
import static name.didier.david.check4j.ConciseCheckers.checkStrictlyPositive;
import static name.didier.david.check4j.FluentCheckers.checkThat;

import java.util.Collection;

/**
 * Validates the documentation code.
 * 
 * @author ddidier
 */
@SuppressWarnings("all")
public class Documentation {

    private String iJustCantBeEmpty;
    private String meNeither;

    public void checkMyParametersWithoutCheck4J(final String iJustCantBeEmpty, final String meNeither) {
        if (iJustCantBeEmpty == null || iJustCantBeEmpty.length() == 0) {
            throw new IllegalArgumentException("iJustCantBeEmpty just can't be empty, told you so!");
        }
        if (meNeither == null || meNeither.length() == 0) {
            throw new IllegalArgumentException("meNeither neither by the way");
        }
        this.iJustCantBeEmpty = iJustCantBeEmpty;
        this.meNeither = meNeither;
    }

    public void checkMyParametersWithCheck4J(final String iJustCantBeEmpty) {
        this.iJustCantBeEmpty = checkThat(iJustCantBeEmpty).isNotEmpty().thenAssign();
        this.meNeither = checkThat(meNeither).as("meNeither").isNotEmpty().thenAssign();
    }

    public void checkMyParametersWithCheck4JButBeConcisePlease(final String iJustCantBeEmpty) {
        this.iJustCantBeEmpty = checkNotEmpty(iJustCantBeEmpty);
        this.meNeither = checkNotEmpty(meNeither, "meNeither");
    }

    // -----------------------------------------------------------------------------------------------------------------

    private Object myObject;
    private String myString;
    private Integer myInteger;
    private Object[] myArray;
    private Collection<Object> myCollection;

    private final Object anObject = new Object();
    private final String aString = new String();
    private final Integer anInt = new Integer(0);
    private final Object[] anArray = new Object[0];
    private final Collection<Object> aCollection = newArrayList();

    public void fluentChecks() {
        this.myObject = checkThat(anObject).as("myObject").isNotNull().thenAssign();
        this.myObject = checkThat(anObject).as("myObject").isInstanceOf(Object.class).thenAssign();

        this.myString = checkThat(aString).as("myString").isNotEmpty().thenAssign();
        this.myString = checkThat(aString).as("myString").isNotBlank().thenAssign();

        this.myInteger = checkThat(anInt).as("myInteger").isNegative().thenAssign();
        this.myInteger = checkThat(anInt).as("myInteger").isStrictlyNegative().thenAssign();
        this.myInteger = checkThat(anInt).as("myInteger").isPositive().thenAssign();
        this.myInteger = checkThat(anInt).as("myInteger").isStrictlyPositive().thenAssign();
        this.myInteger = checkThat(anInt).as("myInteger").isLessThan(1).thenAssign();
        this.myInteger = checkThat(anInt).as("myInteger").isStrictlyLessThan(1).thenAssign();
        this.myInteger = checkThat(anInt).as("myInteger").isGreaterThan(1).thenAssign();
        this.myInteger = checkThat(anInt).as("myInteger").isStrictlyGreaterThan(1).thenAssign();
        this.myInteger = checkThat(anInt).as("myInteger").isBetween(-1, 1).thenAssign();
        this.myInteger = checkThat(anInt).as("myInteger").isStrictlyBetween(-1, 1).thenAssign();

        this.myArray = checkThat(anArray).as("myArray").isNotEmpty().thenAssign();

        this.myCollection = checkThat(aCollection).as("myCollection").isNotEmpty().thenAssign();
    }

    // -----------------------------------------------------------------------------------------------------------------

    public void conciseChecks() {
        this.myObject = checkNotNull(anObject, "myObject");
        this.myObject = checkInstanceOf(anObject, Object.class, "myObject");

        this.myString = checkNotEmpty(aString, "myString");
        this.myString = checkNotBlank(aString, "myString");

        this.myInteger = checkNegative(anInt, "myInteger");
        this.myInteger = checkStrictlyNegative(anInt, "myInteger");
        this.myInteger = checkPositive(anInt, "myInteger");
        this.myInteger = checkStrictlyPositive(anInt, "myInteger");
        this.myInteger = checkLessThan(anInt, 1, "myInteger");
        this.myInteger = checkStrictlyLessThan(anInt, 1, "myInteger");
        this.myInteger = checkGreaterThan(anInt, 1, "myInteger");
        this.myInteger = checkStrictlyGreaterThan(anInt, 1, "myInteger");
        this.myInteger = checkBetween(anInt, -1, 1, "myInteger");
        this.myInteger = checkStrictlyBetween(anInt, -1, 1, "myInteger");

        this.myArray = checkNotEmpty(anArray, "myArray");

        this.myCollection = checkNotEmpty(aCollection, "myCollection");
    }
}
