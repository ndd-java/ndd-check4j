package name.didier.david.check4j;

import static org.assertj.core.api.Assertions.assertThat;

import static name.didier.david.check4j.FluentCheckers.checkThat;

import java.util.ArrayList;
import java.util.Collection;

import org.testng.annotations.Test;

@Test
public class FluentCheckersTest {

    public void constructor_should_not_be_private_to_allow_inheritance() {
        assertThat(new FluentCheckers()).isNotNull();
    }

    public void checkThat_should_return_a_new_ObjectChecker_by_default() {
        assertCheckers(checkThat(new Object()), checkThat(new Object()), FluentObjectChecker.class);
    }

    public void checkThat_should_return_a_new_StringChecker_for_a_string() {
        assertCheckers(checkThat(new String()), checkThat(new String()), FluentStringChecker.class);
    }

    public void checkThat_should_return_a_new_IntegerChecker_for_an_integer() {
        assertCheckers(checkThat(new Integer(0)), checkThat(new Integer(0)), FluentIntegerChecker.class);
    }

    public void checkThat_should_return_a_new_LongChecker_for_a_long() {
        assertCheckers(checkThat(new Long(0)), checkThat(new Long(0)), FluentLongChecker.class);
    }

    public void checkThat_should_return_a_new_FloatIntegerChecker_for_an_float() {
        assertCheckers(checkThat(new Float(0)), checkThat(new Float(0)), FluentFloatChecker.class);
    }

    public void checkThat_should_return_a_new_DoubleChecker_for_a_double() {
        assertCheckers(checkThat(new Double(0)), checkThat(new Double(0)), FluentDoubleChecker.class);
    }

    public void checkThat_should_return_a_new_ObjectArrayChecker_for_an_object_array() {
        assertCheckers(checkThat(new Object[0]), checkThat(new Object[0]), FluentObjectArrayChecker.class);
    }

    public void checkThat_should_return_a_new_CollectionChecker_for_an_object_collection() {
        Collection<Object> collection1 = new ArrayList<>();
        Collection<Object> collection2 = new ArrayList<>();
        assertCheckers(checkThat(collection1), checkThat(collection2), FluentCollectionChecker.class);
    }

    private static <C> void assertCheckers(final C checker1, final C checker2, final Class<C> checkerClass) {
        assertThat(checker1).isInstanceOf(checkerClass);
        assertThat(checker2).isInstanceOf(checkerClass);
        assertThat(checker1).isNotSameAs(checker2);
    }
}
