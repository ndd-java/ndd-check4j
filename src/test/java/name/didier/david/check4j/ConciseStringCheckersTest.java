package name.didier.david.check4j;

import static java.lang.String.format;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;

import static name.didier.david.check4j.ConciseCheckers.checkNotBlank;
import static name.didier.david.check4j.ConciseCheckers.checkNotEmpty;

import org.testng.annotations.Test;

@Test
public class ConciseStringCheckersTest
        extends AbstractConciseCheckersTestBase {

    private static final String NULL = null;
    private static final String STRING = "STRING";
    private static final String EMPTY_STRING = "";
    private static final String BLANK_STRING = "  ";

    // -----------------------------------------------------------------------------------------------------------------

    public void checkNotEmpty_should_pass_if_not_empty_then_return_parameter() {
        assertThat(checkNotEmpty(STRING)).isSameAs(STRING);
    }

    public void checkNotEmpty_should_pass_if_not_empty_then_return_parameter_with_pName() {
        assertThat(checkNotEmpty(STRING, P_NAME)).isSameAs(STRING);
    }

    public void checkNotEmpty_should_pass_if_blank_then_return_parameter() {
        assertThat(checkNotEmpty(BLANK_STRING)).isSameAs(BLANK_STRING);
    }

    public void checkNotEmpty_should_fail_if_null() {
        try {
            checkNotEmpty(NULL);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(NULL_MESSAGE);
        }
    }

    public void checkNotEmpty_should_fail_if_null_with_pName() {
        try {
            checkNotEmpty(NULL, P_NAME);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(NULL_P_MESSAGE);
        }
    }

    public void checkNotEmpty_should_fail_if_empty() {
        try {
            checkNotEmpty(EMPTY_STRING);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Expected parameter to not be empty");
        }
    }

    public void checkNotEmpty_should_fail_if_empty_with_pName() {
        try {
            checkNotEmpty(EMPTY_STRING, P_NAME);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Expected parameter 'P' to not be empty");
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    public void checkNotBlank_should_pass_if_not_blank_then_return_parameter() {
        assertThat(checkNotBlank(STRING)).isSameAs(STRING);
    }

    public void checkNotBlank_should_pass_if_not_blank_then_return_parameter_with_pName() {
        assertThat(checkNotBlank(STRING, P_NAME)).isSameAs(STRING);
    }

    public void checkNotBlank_should_fail_if_null() {
        try {
            checkNotBlank(NULL);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(NULL_MESSAGE);
        }
    }

    public void checkNotBlank_should_fail_if_null_with_pName() {
        try {
            checkNotBlank(NULL, P_NAME);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(NULL_P_MESSAGE);
        }
    }

    public void checkNotBlank_should_fail_if_empty() {
        try {
            checkNotBlank(EMPTY_STRING);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format("Expected parameter to not be blank but was <%s>", EMPTY_STRING));
        }
    }

    public void checkNotBlank_should_fail_if_empty_with_pName() {
        try {
            checkNotBlank(EMPTY_STRING, P_NAME);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format("Expected parameter 'P' to not be blank but was <%s>", EMPTY_STRING));
        }
    }

    public void checkNotBlank_should_fail_if_blank() {
        try {
            checkNotBlank(BLANK_STRING);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format("Expected parameter to not be blank but was <%s>", BLANK_STRING));
        }
    }

    public void checkNotBlank_should_fail_if_blank_with_pName() {
        try {
            checkNotBlank(BLANK_STRING, P_NAME);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format("Expected parameter 'P' to not be blank but was <%s>", BLANK_STRING));
        }
    }

}
