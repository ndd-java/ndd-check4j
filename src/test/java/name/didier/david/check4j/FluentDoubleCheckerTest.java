package name.didier.david.check4j;

import static java.lang.Double.valueOf;

import org.testng.annotations.Test;

@Test
public class FluentDoubleCheckerTest
        extends AbstractFluentNumberCheckerTestBase<FluentDoubleChecker, Double> {

    // -----------------------------------------------------------------------------------------------------------------

    @Override
    protected FluentDoubleChecker newChecker(final Double actual) {
        return new FluentDoubleChecker(actual);
    }

    @Override
    protected Double numberN() {
        return valueOf(-1);
    }

    @Override
    protected Double numberZ() {
        return valueOf(0);
    }

    @Override
    protected Double numberP() {
        return valueOf(1);
    }
}
