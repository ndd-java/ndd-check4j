package name.didier.david.check4j;

import static org.assertj.core.api.Assertions.assertThat;

import org.testng.annotations.Test;

@Test
public class ConciseCheckersTest
        extends AbstractConciseCheckersTestBase {

    public void constructor_should_not_be_private_to_allow_inheritance() {
        assertThat(new ConciseCheckers()).isNotNull();
    }

}
