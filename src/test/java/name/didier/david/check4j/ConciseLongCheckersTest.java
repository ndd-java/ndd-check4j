package name.didier.david.check4j;

import static java.lang.Long.valueOf;

import org.testng.annotations.Test;

@Test
public class ConciseLongCheckersTest
        extends AbstractConciseNumberCheckersTestBase<Long> {

    @Override
    protected Long checkNegative(final Long numberA) {
        return ConciseCheckers.checkNegative(numberA);
    }

    @Override
    protected Long checkNegative(final Long numberA, final String pName) {
        return ConciseCheckers.checkNegative(numberA, pName);
    }

    @Override
    protected Long checkStrictlyNegative(final Long numberN) {
        return ConciseCheckers.checkStrictlyNegative(numberN);
    }

    @Override
    protected Long checkStrictlyNegative(final Long numberN, final String pName) {
        return ConciseCheckers.checkStrictlyNegative(numberN, pName);
    }

    @Override
    protected Long checkPositive(final Long numberN) {
        return ConciseCheckers.checkPositive(numberN);
    }

    @Override
    protected Long checkPositive(final Long numberN, final String pName) {
        return ConciseCheckers.checkPositive(numberN, pName);
    }

    @Override
    protected Long checkStrictlyPositive(final Long numberN) {
        return ConciseCheckers.checkStrictlyPositive(numberN);
    }

    @Override
    protected Long checkStrictlyPositive(final Long numberN, final String pName) {
        return ConciseCheckers.checkStrictlyPositive(numberN, pName);
    }

    @Override
    protected Long checkLessThan(final Long numberN, final Number max) {
        return ConciseCheckers.checkLessThan(numberN, max);
    }

    @Override
    protected Long checkLessThan(final Long numberN, final Number max, final String pName) {
        return ConciseCheckers.checkLessThan(numberN, max, pName);
    }

    @Override
    protected Long checkStrictlyLessThan(final Long numberN, final Number max) {
        return ConciseCheckers.checkStrictlyLessThan(numberN, max);
    }

    @Override
    protected Long checkStrictlyLessThan(final Long numberN, final Number max, final String pName) {
        return ConciseCheckers.checkStrictlyLessThan(numberN, max, pName);
    }

    @Override
    protected Long checkGreaterThan(final Long numberN, final Number min) {
        return ConciseCheckers.checkGreaterThan(numberN, min);
    }

    @Override
    protected Long checkGreaterThan(final Long numberN, final Number min, final String pName) {
        return ConciseCheckers.checkGreaterThan(numberN, min, pName);
    }

    @Override
    protected Long checkStrictlyGreaterThan(final Long numberN, final Number min) {
        return ConciseCheckers.checkStrictlyGreaterThan(numberN, min);
    }

    @Override
    protected Long checkStrictlyGreaterThan(final Long numberN, final Number min, final String pName) {
        return ConciseCheckers.checkStrictlyGreaterThan(numberN, min, pName);
    }

    @Override
    protected Long checkBetween(final Long numberN, final Number min, final Number max) {
        return ConciseCheckers.checkBetween(numberN, min, max);
    }

    @Override
    protected Long checkBetween(final Long numberN, final Number min, final Number max, final String pName) {
        return ConciseCheckers.checkBetween(numberN, min, max, pName);
    }

    @Override
    protected Long checkStrictlyBetween(final Long numberN, final Number min, final Number max) {
        return ConciseCheckers.checkStrictlyBetween(numberN, min, max);
    }

    @Override
    protected Long checkStrictlyBetween(final Long numberN, final Number min, final Number max, final String pName) {
        return ConciseCheckers.checkStrictlyBetween(numberN, min, max, pName);
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Override
    protected Long numberN() {
        return valueOf(-1);
    }

    @Override
    protected Long numberZ() {
        return valueOf(0);
    }

    @Override
    protected Long numberP() {
        return valueOf(1);
    }

}
