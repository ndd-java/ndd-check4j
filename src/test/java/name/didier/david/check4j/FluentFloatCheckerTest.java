package name.didier.david.check4j;

import static java.lang.Float.valueOf;

import org.testng.annotations.Test;

@Test
public class FluentFloatCheckerTest
        extends AbstractFluentNumberCheckerTestBase<FluentFloatChecker, Float> {

    // -----------------------------------------------------------------------------------------------------------------

    @Override
    protected FluentFloatChecker newChecker(final Float actual) {
        return new FluentFloatChecker(actual);
    }

    @Override
    protected Float numberN() {
        return valueOf(-1);
    }

    @Override
    protected Float numberZ() {
        return valueOf(0);
    }

    @Override
    protected Float numberP() {
        return valueOf(1);
    }
}
