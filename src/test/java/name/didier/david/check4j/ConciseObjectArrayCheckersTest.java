package name.didier.david.check4j;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;

import static name.didier.david.check4j.ConciseCheckers.checkNotEmpty;

import org.testng.annotations.Test;

@Test
public class ConciseObjectArrayCheckersTest
        extends AbstractConciseCheckersTestBase {

    private static final Object[] NULL = null;
    private static final Object[] EMPTY = new Object[0];
    private static final Object[] NOT_EMPTY = new Object[] { new Object() };

    // -----------------------------------------------------------------------------------------------------------------

    public void checkNotEmpty_should_pass_if_not_empty_then_return_parameter() {
        assertThat(checkNotEmpty(NOT_EMPTY)).isSameAs(NOT_EMPTY);
    }

    public void checkNotEmpty_should_pass_if_not_empty_then_return_parameter_with_pName() {
        assertThat(checkNotEmpty(NOT_EMPTY, P_NAME)).isSameAs(NOT_EMPTY);
    }

    public void checkNotEmpty_should_fail_if_null() {
        try {
            checkNotEmpty(NULL);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(NULL_MESSAGE);
        }
    }

    public void checkNotEmpty_should_fail_if_null_with_pName() {
        try {
            checkNotEmpty(NULL, P_NAME);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(NULL_P_MESSAGE);
        }
    }

    public void checkNotEmpty_should_fail_if_empty() {
        try {
            checkNotEmpty(EMPTY);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Expected parameter to not be empty");
        }
    }

    public void checkNotEmpty_should_fail_if_empty_with_pName() {
        try {
            checkNotEmpty(EMPTY, P_NAME);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Expected parameter 'P' to not be empty");
        }
    }

}
