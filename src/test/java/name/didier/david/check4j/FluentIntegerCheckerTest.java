package name.didier.david.check4j;

import static java.lang.Integer.valueOf;

import org.testng.annotations.Test;

@Test
public class FluentIntegerCheckerTest
        extends AbstractFluentNumberCheckerTestBase<FluentIntegerChecker, Integer> {

    // -----------------------------------------------------------------------------------------------------------------

    @Override
    protected FluentIntegerChecker newChecker(final Integer actual) {
        return new FluentIntegerChecker(actual);
    }

    @Override
    protected Integer numberN() {
        return valueOf(-1);
    }

    @Override
    protected Integer numberZ() {
        return valueOf(0);
    }

    @Override
    protected Integer numberP() {
        return valueOf(1);
    }
}
