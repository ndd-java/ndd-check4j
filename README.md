# Check4J

Check4J enables easy parameters checking with a fluent or a concise API. Released under the [LGPL](https://www.gnu.org/licenses/lgpl.html).

## Usage

Before, aka the messy version:

```java
public void checkMyParametersWithoutCheck4J(final String iJustCantBeEmpty, String meNeither) {
    if (iJustCantBeEmpty == null || iJustCantBeEmpty.length() == 0) {
        throw new IllegalArgumentException("iJustCantBeEmpty just can't be empty, told you so!");
    }
    if (meNeither == null || meNeither.length() == 0) {
        throw new IllegalArgumentException("meNeither neither by the way");
    }
    this.iJustCantBeEmpty = iJustCantBeEmpty;
    this.meNeither = meNeither;
}
```

After, using the **fluent version** (see the [documentation](https://ndd-java.bitbucket.org/ndd-check4j/fluent-checkers.html)):

```java
import static name.didier.david.check4j.FluentCheckers.checkThat;

public void checkMyParametersWithCheck4J(final String iJustCantBeEmpty) {
    this.iJustCantBeEmpty = checkThat(iJustCantBeEmpty).isNotEmpty().thenAssign();
    this.meNeither        = checkThat(meNeither).as("meNeither").isNotEmpty().thenAssign();
}
```

`checkThat()` creates a new `Checker` upon which you can chain any validation method such as `isNotEmpty()`. If you want the message to reference the parameter name, you must use `as()` since there is no other way to get it. If you want to assign the parameter, which is a quite common thing, you must call `thenAssign()` at the end of the chain, effectively terminating it.


If this is too verbose for you, there is a **concise version** (see the [documentation](https://ndd-java.bitbucket.org/ndd-check4j/concise-checkers.html)):

```java
public void checkMyParametersWithCheck4JButBeConcisePlease(final String iJustCantBeEmpty) {
    this.iJustCantBeEmpty = checkNotEmpty(iJustCantBeEmpty);
    this.meNeither = checkNotEmpty(meNeither, "meNeither");
}
```

For the time being, only very basic checks are provided, but they should cover at least 80% of real life use cases and other will be added...

## Setup

Using Maven:

```xml
<dependency>
  <groupId>name.didier.david</groupId>
  <artifactId>ndd-check4j</artifactId>
  <version>X.Y.Z</version>
</dependency>
```

## Reports

Maven reports are available at [https://ndd-java.bitbucket.org/ndd-check4j/project-reports.html](https://ndd-java.bitbucket.org/ndd-check4j/project-reports.html)

## About

Thanks to the [AssertJ](http://joel-costigliola.github.io/assertj/) contributors which inspired this library.

Copyright David DIDIER 2014
